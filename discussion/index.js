// create a standard server app

// get the http module using the require() directive. repackage the module on a new variable
	const http = require('http');

	const host = 4000;
// create the server and place it inside a new variable to give it an identifier.
	
	// Arguments => they are used to "catch" data and pass it along inside our function
	let server = http.createServer( (req, res) => {
		res.end('Welcome to the App');

	});

// Assign a designated port that will searve the project, by binding the connection with the desired port.

	server.listen(host);

// include a response that will be displayed in the console to verify the connection that was established
	console.log(`Listening on port: ${host}`);